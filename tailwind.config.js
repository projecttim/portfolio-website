/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/*.html", "./src/**/*.js"],
  theme: {
    colors: {
      TBlack: "#020202",
      TDarkGreen: "#0D2818",
      TMidGreen: "#04471C",
      TLightGreen: "#058C42",
      TFontColor: "#16DB65",
    },
    extend: {
      dropShadow: {
        '3xl': '0 35px 35px rgba(0, 0, 0, 0.25)',
        '4xl': [
            '0 35px 35px rgba(0, 0, 0, 0.25)',
            '0 45px 65px rgba(0, 0, 0, 0.15)'
        ]
      }
    },
  },
  plugins: [],
}

