const version = "v2.0.0";
const topBar = document.querySelector('#top');
const header = document.querySelector('header');
const fliepeltjes = document.querySelector('#fliepeltjes');

document.querySelector(".version").innerHTML = version;

window.addEventListener('scroll', function() {
    header.classList.remove('startAnimation');
    if (window.scrollY < topBar.offsetHeight + 20) {
        header.classList.add('is-stuck');
    } else {
        header.classList.remove('is-stuck');
    }
    fliepeltjes.style.transform = `translateY(-${window.scrollY / 5}px)`;
});

function scrollToSection() {
    const section = document.body;
    section.scrollIntoView({behavior: "smooth"});
}

const bars = document.querySelectorAll('.progressBar');

const observer = new IntersectionObserver(entries => {
    entries.forEach(entry => {
        entry.target.classList.toggle("addAnimation", entry.isIntersecting);
    });
});

bars.forEach(bar => {
    observer.observe(bar);
});